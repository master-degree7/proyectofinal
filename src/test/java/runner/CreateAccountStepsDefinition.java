package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.MainPage;
import page.MenuSection;
import page.NewAccountModal;
import session.Session;

import java.util.Map;
import java.util.Random;

public class CreateAccountStepsDefinition {

    MainPage mainPage = new MainPage();
    NewAccountModal newAccountModal = new NewAccountModal();
    MenuSection menuSection = new MenuSection();

    @Given("the {string} page")
    public void thePage(String url) {
        Session.getInstance().getBrowser().get(url);
    }

    @And("open modal option in order to create a new account")
    public void createANewAccount() {
        mainPage.newAccountButton.click();
    }

    @And("use this data in order to create a new account")
    public void useThisDataInOrderToCreateANewAccount(Map<String, String> newAccountData) {
        Random random = new Random();
        int value = random.nextInt(100 + 10) + 10;
        String email = String.format("%s%s%s", newAccountData.get("email"), value, "@gmail.com");

        newAccountModal.fullNameTxtBox.writeText(newAccountData.get("fullName"));
        newAccountModal.emailTxtBox.writeText(email);
        newAccountModal.passwordTxtBox.writeText(newAccountData.get("password"));
        newAccountModal.checkBox.click();

    }

    @And("click on signup button")
    public void clickOnSignupButton() {
        mainPage.signupButton.click();
    }

    @Then("the account is created")
    public void theAccountIsCreated() {
        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "ERROR login failed");
    }
}
