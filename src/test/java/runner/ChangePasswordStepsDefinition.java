package runner;

import file.PasswordGenerator;
import file.ReadFile;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.ChangePasswordModal;
import page.LoginModal;
import page.MainPage;
import page.MenuSection;
import session.Session;

import java.util.Map;

import static file.PasswordGenerator.getNewPassword;

public class ChangePasswordStepsDefinition {

    MainPage mainPage = new MainPage();
    MenuSection menuSection = new MenuSection();
    LoginModal loginModal = new LoginModal();
    ChangePasswordModal changePasswordModal = new ChangePasswordModal();
    ReadFile readFile = new ReadFile();



    @Given("the {string} page opened")
    public void theTodoLyPageOpened(String url, Map<String, String> credential) {
        Session.getInstance().getBrowser().get(url);
        String password = readFile.readingFile();

        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(credential.get("email"));
        loginModal.pwdTxtBox.writeText(password);
        loginModal.loginButton.click();
    }

    @When("open modal to change password")
    public void openModalToChangePassword() {
        menuSection.settingButton.click();
    }

    @And("change password")
    public void useTheNextPasswordToChange() {
        String oldPassword = readFile.readingFile();
        String newPassword = getNewPassword(10);

        changePasswordModal.oldPasswordTxtBox.writeText(oldPassword);
        changePasswordModal.newPasswordTxtBox.writeText(newPassword);

        readFile.writeFile(newPassword);
    }

    @And("click on OK button and logout")
    public void clickOnOKButton() {
        mainPage.changePasswordButton.click();
        mainPage.logoutButton.click();
    }

    @Then("the login should work with the new password")
    public void theLoginShouldWorkWithTheNewPassword(Map<String, String> credential) {
        String password = readFile.readingFile();

        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText(credential.get("email"));
        loginModal.pwdTxtBox.writeText(password);
        loginModal.loginButton.click();

        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "ERROR login failed");
    }
}
