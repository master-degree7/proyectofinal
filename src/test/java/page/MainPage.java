package page;

import control.Button;
import org.openqa.selenium.By;

public class MainPage {
    public Button newAccountButton = new Button(By.xpath("//img[@src='/Images/design/pagesignup.png']"));
    public Button signupButton = new Button(By.id("ctl00_MainContent_SignupControl1_ButtonSignup"));
    public Button loginButton = new Button(By.xpath("//img[@src='/Images/design/pagelogin.png']"));
    public Button changePasswordButton = new Button(By.xpath("/html/body/div[9]/div[2]/div/button[1]"));
    public Button logoutButton = new Button(By.id("ctl00_HeaderTopControl1_LinkButtonLogout"));

}
