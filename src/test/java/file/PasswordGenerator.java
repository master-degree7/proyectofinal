package file;

public class PasswordGenerator {
    public static String NUMEROS = "0123456789";

    public static String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";

    public static String ESPECIALES = "ñÑ";

    //
    public static String getPinNumber() {
        return getNewPassword(NUMEROS, 4);
    }

    public static String getPassword() {
        return getNewPassword(8);
    }

    public static String getNewPassword(int length) {
        return getNewPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length);
    }

    public static String getNewPassword(String key, int length) {
        String pswd = "";

        for (int i = 0; i < length; i++) {
            pswd+=(key.charAt((int)(Math.random() * key.length())));
        }

        return pswd;
    }
}
