package file;

import java.io.*;

public class ReadFile {

    public String readingFile() {
        String text = null;
        try {
            File file = new File(getPath("config.txt"));
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null)
                text = st;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public void writeFile(String toWrite) {
        try {
            FileWriter fw = new FileWriter(getPath("config.txt"));
            fw.write(toWrite);
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Success...");

    }

//    public static void main(String[] args) {
//        ReadFile readFile = new ReadFile();
//        readFile.writeFile("ABELCOPA");
//    }

    public String getPath(String fileName) {
        String userDir = System.getProperty("user.dir");
        String filePath = String.format("%s%s%s", userDir, "\\src\\main\\resources\\", fileName);

        return filePath;
    }
}
