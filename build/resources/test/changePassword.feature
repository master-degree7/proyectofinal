Feature: ChangePassword

  Scenario: Enter to  http://todo.ly/ and change password
    Given the "http://todo.ly/" page opened
      | email    | abelw.copa.torrez@gmail.com |
    When open modal to change password
    And change password
    And click on OK button and logout
    Then the login should work with the new password
      | email    | abelw.copa.torrez@gmail.com |