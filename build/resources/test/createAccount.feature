Feature: CreateAccount

  Scenario: Enter to  http://todo.ly/ and create an account
    Given the "http://todo.ly/" page
    And open modal option in order to create a new account
    And use this data in order to create a new account
      | fullName | William Copa  |
      | email    | william.copa |
      | password | 123abc     |
    And click on signup button
    Then the account is created
